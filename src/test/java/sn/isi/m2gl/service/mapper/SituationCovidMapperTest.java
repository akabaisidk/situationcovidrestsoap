package sn.isi.m2gl.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class SituationCovidMapperTest {

    private SituationCovidMapper situationCovidMapper;

    @BeforeEach
    public void setUp() {
        situationCovidMapper = new SituationCovidMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(situationCovidMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(situationCovidMapper.fromId(null)).isNull();
    }
}
