package sn.isi.m2gl.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.isi.m2gl.web.rest.TestUtil;

public class SituationCovidDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SituationCovidDTO.class);
        SituationCovidDTO situationCovidDTO1 = new SituationCovidDTO();
        situationCovidDTO1.setId(1L);
        SituationCovidDTO situationCovidDTO2 = new SituationCovidDTO();
        assertThat(situationCovidDTO1).isNotEqualTo(situationCovidDTO2);
        situationCovidDTO2.setId(situationCovidDTO1.getId());
        assertThat(situationCovidDTO1).isEqualTo(situationCovidDTO2);
        situationCovidDTO2.setId(2L);
        assertThat(situationCovidDTO1).isNotEqualTo(situationCovidDTO2);
        situationCovidDTO1.setId(null);
        assertThat(situationCovidDTO1).isNotEqualTo(situationCovidDTO2);
    }
}
