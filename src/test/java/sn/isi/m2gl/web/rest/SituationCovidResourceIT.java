package sn.isi.m2gl.web.rest;

import sn.isi.m2gl.SituationCovidRestSoapApp;
import sn.isi.m2gl.config.TestSecurityConfiguration;
import sn.isi.m2gl.domain.SituationCovid;
import sn.isi.m2gl.repository.SituationCovidRepository;
import sn.isi.m2gl.service.SituationCovidService;
import sn.isi.m2gl.service.dto.SituationCovidDTO;
import sn.isi.m2gl.service.mapper.SituationCovidMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static sn.isi.m2gl.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SituationCovidResource} REST controller.
 */
@SpringBootTest(classes = { SituationCovidRestSoapApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class SituationCovidResourceIT {

    private static final Integer DEFAULT_NBRE_TESTS_EFFECTUES = 1;
    private static final Integer UPDATED_NBRE_TESTS_EFFECTUES = 2;

    private static final Integer DEFAULT_NBRE_CAS_POSITIFS = 1;
    private static final Integer UPDATED_NBRE_CAS_POSITIFS = 2;

    private static final Integer DEFAULT_NBRE_CAS_IMPORTES = 1;
    private static final Integer UPDATED_NBRE_CAS_IMPORTES = 2;

    private static final Integer DEFAULT_NBRE_DECES = 1;
    private static final Integer UPDATED_NBRE_DECES = 2;

    private static final Integer DEFAULT_NBRE_GUERIS = 1;
    private static final Integer UPDATED_NBRE_GUERIS = 2;

    private static final ZonedDateTime DEFAULT_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private SituationCovidRepository situationCovidRepository;

    @Autowired
    private SituationCovidMapper situationCovidMapper;

    @Autowired
    private SituationCovidService situationCovidService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSituationCovidMockMvc;

    private SituationCovid situationCovid;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SituationCovid createEntity(EntityManager em) {
        SituationCovid situationCovid = new SituationCovid()
            .nbre_tests_effectues(DEFAULT_NBRE_TESTS_EFFECTUES)
            .nbre_cas_positifs(DEFAULT_NBRE_CAS_POSITIFS)
            .nbre_cas_importes(DEFAULT_NBRE_CAS_IMPORTES)
            .nbre_deces(DEFAULT_NBRE_DECES)
            .nbre_gueris(DEFAULT_NBRE_GUERIS)
            .date(DEFAULT_DATE);
        return situationCovid;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SituationCovid createUpdatedEntity(EntityManager em) {
        SituationCovid situationCovid = new SituationCovid()
            .nbre_tests_effectues(UPDATED_NBRE_TESTS_EFFECTUES)
            .nbre_cas_positifs(UPDATED_NBRE_CAS_POSITIFS)
            .nbre_cas_importes(UPDATED_NBRE_CAS_IMPORTES)
            .nbre_deces(UPDATED_NBRE_DECES)
            .nbre_gueris(UPDATED_NBRE_GUERIS)
            .date(UPDATED_DATE);
        return situationCovid;
    }

    @BeforeEach
    public void initTest() {
        situationCovid = createEntity(em);
    }

    @Test
    @Transactional
    public void createSituationCovid() throws Exception {
        int databaseSizeBeforeCreate = situationCovidRepository.findAll().size();
        // Create the SituationCovid
        SituationCovidDTO situationCovidDTO = situationCovidMapper.toDto(situationCovid);
        restSituationCovidMockMvc.perform(post("/api/situation-covids").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(situationCovidDTO)))
            .andExpect(status().isCreated());

        // Validate the SituationCovid in the database
        List<SituationCovid> situationCovidList = situationCovidRepository.findAll();
        assertThat(situationCovidList).hasSize(databaseSizeBeforeCreate + 1);
        SituationCovid testSituationCovid = situationCovidList.get(situationCovidList.size() - 1);
        assertThat(testSituationCovid.getNbre_tests_effectues()).isEqualTo(DEFAULT_NBRE_TESTS_EFFECTUES);
        assertThat(testSituationCovid.getNbre_cas_positifs()).isEqualTo(DEFAULT_NBRE_CAS_POSITIFS);
        assertThat(testSituationCovid.getNbre_cas_importes()).isEqualTo(DEFAULT_NBRE_CAS_IMPORTES);
        assertThat(testSituationCovid.getNbre_deces()).isEqualTo(DEFAULT_NBRE_DECES);
        assertThat(testSituationCovid.getNbre_gueris()).isEqualTo(DEFAULT_NBRE_GUERIS);
        assertThat(testSituationCovid.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    @Transactional
    public void createSituationCovidWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = situationCovidRepository.findAll().size();

        // Create the SituationCovid with an existing ID
        situationCovid.setId(1L);
        SituationCovidDTO situationCovidDTO = situationCovidMapper.toDto(situationCovid);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSituationCovidMockMvc.perform(post("/api/situation-covids").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(situationCovidDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SituationCovid in the database
        List<SituationCovid> situationCovidList = situationCovidRepository.findAll();
        assertThat(situationCovidList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNbre_tests_effectuesIsRequired() throws Exception {
        int databaseSizeBeforeTest = situationCovidRepository.findAll().size();
        // set the field null
        situationCovid.setNbre_tests_effectues(null);

        // Create the SituationCovid, which fails.
        SituationCovidDTO situationCovidDTO = situationCovidMapper.toDto(situationCovid);


        restSituationCovidMockMvc.perform(post("/api/situation-covids").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(situationCovidDTO)))
            .andExpect(status().isBadRequest());

        List<SituationCovid> situationCovidList = situationCovidRepository.findAll();
        assertThat(situationCovidList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNbre_cas_positifsIsRequired() throws Exception {
        int databaseSizeBeforeTest = situationCovidRepository.findAll().size();
        // set the field null
        situationCovid.setNbre_cas_positifs(null);

        // Create the SituationCovid, which fails.
        SituationCovidDTO situationCovidDTO = situationCovidMapper.toDto(situationCovid);


        restSituationCovidMockMvc.perform(post("/api/situation-covids").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(situationCovidDTO)))
            .andExpect(status().isBadRequest());

        List<SituationCovid> situationCovidList = situationCovidRepository.findAll();
        assertThat(situationCovidList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNbre_cas_importesIsRequired() throws Exception {
        int databaseSizeBeforeTest = situationCovidRepository.findAll().size();
        // set the field null
        situationCovid.setNbre_cas_importes(null);

        // Create the SituationCovid, which fails.
        SituationCovidDTO situationCovidDTO = situationCovidMapper.toDto(situationCovid);


        restSituationCovidMockMvc.perform(post("/api/situation-covids").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(situationCovidDTO)))
            .andExpect(status().isBadRequest());

        List<SituationCovid> situationCovidList = situationCovidRepository.findAll();
        assertThat(situationCovidList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNbre_decesIsRequired() throws Exception {
        int databaseSizeBeforeTest = situationCovidRepository.findAll().size();
        // set the field null
        situationCovid.setNbre_deces(null);

        // Create the SituationCovid, which fails.
        SituationCovidDTO situationCovidDTO = situationCovidMapper.toDto(situationCovid);


        restSituationCovidMockMvc.perform(post("/api/situation-covids").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(situationCovidDTO)))
            .andExpect(status().isBadRequest());

        List<SituationCovid> situationCovidList = situationCovidRepository.findAll();
        assertThat(situationCovidList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNbre_guerisIsRequired() throws Exception {
        int databaseSizeBeforeTest = situationCovidRepository.findAll().size();
        // set the field null
        situationCovid.setNbre_gueris(null);

        // Create the SituationCovid, which fails.
        SituationCovidDTO situationCovidDTO = situationCovidMapper.toDto(situationCovid);


        restSituationCovidMockMvc.perform(post("/api/situation-covids").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(situationCovidDTO)))
            .andExpect(status().isBadRequest());

        List<SituationCovid> situationCovidList = situationCovidRepository.findAll();
        assertThat(situationCovidList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = situationCovidRepository.findAll().size();
        // set the field null
        situationCovid.setDate(null);

        // Create the SituationCovid, which fails.
        SituationCovidDTO situationCovidDTO = situationCovidMapper.toDto(situationCovid);


        restSituationCovidMockMvc.perform(post("/api/situation-covids").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(situationCovidDTO)))
            .andExpect(status().isBadRequest());

        List<SituationCovid> situationCovidList = situationCovidRepository.findAll();
        assertThat(situationCovidList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSituationCovids() throws Exception {
        // Initialize the database
        situationCovidRepository.saveAndFlush(situationCovid);

        // Get all the situationCovidList
        restSituationCovidMockMvc.perform(get("/api/situation-covids?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(situationCovid.getId().intValue())))
            .andExpect(jsonPath("$.[*].nbre_tests_effectues").value(hasItem(DEFAULT_NBRE_TESTS_EFFECTUES)))
            .andExpect(jsonPath("$.[*].nbre_cas_positifs").value(hasItem(DEFAULT_NBRE_CAS_POSITIFS)))
            .andExpect(jsonPath("$.[*].nbre_cas_importes").value(hasItem(DEFAULT_NBRE_CAS_IMPORTES)))
            .andExpect(jsonPath("$.[*].nbre_deces").value(hasItem(DEFAULT_NBRE_DECES)))
            .andExpect(jsonPath("$.[*].nbre_gueris").value(hasItem(DEFAULT_NBRE_GUERIS)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(sameInstant(DEFAULT_DATE))));
    }
    
    @Test
    @Transactional
    public void getSituationCovid() throws Exception {
        // Initialize the database
        situationCovidRepository.saveAndFlush(situationCovid);

        // Get the situationCovid
        restSituationCovidMockMvc.perform(get("/api/situation-covids/{id}", situationCovid.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(situationCovid.getId().intValue()))
            .andExpect(jsonPath("$.nbre_tests_effectues").value(DEFAULT_NBRE_TESTS_EFFECTUES))
            .andExpect(jsonPath("$.nbre_cas_positifs").value(DEFAULT_NBRE_CAS_POSITIFS))
            .andExpect(jsonPath("$.nbre_cas_importes").value(DEFAULT_NBRE_CAS_IMPORTES))
            .andExpect(jsonPath("$.nbre_deces").value(DEFAULT_NBRE_DECES))
            .andExpect(jsonPath("$.nbre_gueris").value(DEFAULT_NBRE_GUERIS))
            .andExpect(jsonPath("$.date").value(sameInstant(DEFAULT_DATE)));
    }
    @Test
    @Transactional
    public void getNonExistingSituationCovid() throws Exception {
        // Get the situationCovid
        restSituationCovidMockMvc.perform(get("/api/situation-covids/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSituationCovid() throws Exception {
        // Initialize the database
        situationCovidRepository.saveAndFlush(situationCovid);

        int databaseSizeBeforeUpdate = situationCovidRepository.findAll().size();

        // Update the situationCovid
        SituationCovid updatedSituationCovid = situationCovidRepository.findById(situationCovid.getId()).get();
        // Disconnect from session so that the updates on updatedSituationCovid are not directly saved in db
        em.detach(updatedSituationCovid);
        updatedSituationCovid
            .nbre_tests_effectues(UPDATED_NBRE_TESTS_EFFECTUES)
            .nbre_cas_positifs(UPDATED_NBRE_CAS_POSITIFS)
            .nbre_cas_importes(UPDATED_NBRE_CAS_IMPORTES)
            .nbre_deces(UPDATED_NBRE_DECES)
            .nbre_gueris(UPDATED_NBRE_GUERIS)
            .date(UPDATED_DATE);
        SituationCovidDTO situationCovidDTO = situationCovidMapper.toDto(updatedSituationCovid);

        restSituationCovidMockMvc.perform(put("/api/situation-covids").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(situationCovidDTO)))
            .andExpect(status().isOk());

        // Validate the SituationCovid in the database
        List<SituationCovid> situationCovidList = situationCovidRepository.findAll();
        assertThat(situationCovidList).hasSize(databaseSizeBeforeUpdate);
        SituationCovid testSituationCovid = situationCovidList.get(situationCovidList.size() - 1);
        assertThat(testSituationCovid.getNbre_tests_effectues()).isEqualTo(UPDATED_NBRE_TESTS_EFFECTUES);
        assertThat(testSituationCovid.getNbre_cas_positifs()).isEqualTo(UPDATED_NBRE_CAS_POSITIFS);
        assertThat(testSituationCovid.getNbre_cas_importes()).isEqualTo(UPDATED_NBRE_CAS_IMPORTES);
        assertThat(testSituationCovid.getNbre_deces()).isEqualTo(UPDATED_NBRE_DECES);
        assertThat(testSituationCovid.getNbre_gueris()).isEqualTo(UPDATED_NBRE_GUERIS);
        assertThat(testSituationCovid.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingSituationCovid() throws Exception {
        int databaseSizeBeforeUpdate = situationCovidRepository.findAll().size();

        // Create the SituationCovid
        SituationCovidDTO situationCovidDTO = situationCovidMapper.toDto(situationCovid);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSituationCovidMockMvc.perform(put("/api/situation-covids").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(situationCovidDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SituationCovid in the database
        List<SituationCovid> situationCovidList = situationCovidRepository.findAll();
        assertThat(situationCovidList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSituationCovid() throws Exception {
        // Initialize the database
        situationCovidRepository.saveAndFlush(situationCovid);

        int databaseSizeBeforeDelete = situationCovidRepository.findAll().size();

        // Delete the situationCovid
        restSituationCovidMockMvc.perform(delete("/api/situation-covids/{id}", situationCovid.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SituationCovid> situationCovidList = situationCovidRepository.findAll();
        assertThat(situationCovidList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
