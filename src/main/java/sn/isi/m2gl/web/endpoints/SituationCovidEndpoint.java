package sn.isi.m2gl.web.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import sn.isi.m2gl.domain.SituationCovid;
import sn.isi.m2gl.repository.SituationCovidRepository;
import sn.isi.m2gl.wsdl.GetInfoRequest;
import sn.isi.m2gl.wsdl.GetInfoResponse;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.util.GregorianCalendar;
import java.util.Optional;

@Endpoint
public class SituationCovidEndpoint {

    @Autowired
    SituationCovidRepository situationCovidRepository;

    DatatypeFactory datatypeFactory;

    {
        try {
            datatypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
    }

    GregorianCalendar calendarExpected = new GregorianCalendar(2018, 6, 28);
    XMLGregorianCalendar expectedXMLGregorianCalendar = datatypeFactory
        .newXMLGregorianCalendar(calendarExpected);

    LocalDate localDate = LocalDate.of(2019, 4, 25);

    XMLGregorianCalendar xmlGregorianCalendar =
        datatypeFactory.newXMLGregorianCalendar(localDate.toString());




    @PayloadRoot(namespace = "http://ws.groupeisi.com",localPart = "getInfoRequest")
    public @ResponsePayload
    GetInfoResponse getInfoRequest (@RequestPayload GetInfoRequest request) throws DatatypeConfigurationException {
        GetInfoResponse covidresponse= new GetInfoResponse();
        long bb = Long.parseLong(request.getInput());
        System.out.println("Input");
        System.out.println(bb);
        //Sencovid sencovid = sencovidRepository.getCovidInfoById((long)11);
        Optional<SituationCovid> sencovid = situationCovidRepository.findById(bb);
        XMLGregorianCalendar dateFormat =
            datatypeFactory.newXMLGregorianCalendar(sencovid.get().getDate().toString());
        covidresponse.setNbre_tests_effectues(sencovid.get().getNbre_tests_effectues());
        covidresponse.setNbre_cas_positifs(sencovid.get().getNbre_cas_positifs());
        covidresponse.setNbre_cas_importes(sencovid.get().getNbre_cas_importes());
        covidresponse.setNbre_deces(sencovid.get().getNbre_deces());
        covidresponse.setNbre_gueris(sencovid.get().getNbre_gueris());
        covidresponse.setDate(dateFormat);
        return covidresponse;
    }

}
