//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.11 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2021.02.16 à 01:40:14 PM UTC
//


package sn.isi.m2gl.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour anonymous complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nbrtest" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="postifcase" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="importedCase" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="death" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="recovered" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "nbre_tests_effectues",
    "nbre_cas_positifs",
    "nbre_cas_importes",
    "nbre_deces",
    "nbre_gueris",
    "date"
})
@XmlRootElement(name = "getInfoResponse")
public class GetInfoResponse {

    @XmlElement(required = true)
    protected Integer nbre_tests_effectues;
    @XmlElement(required = true)
    protected Integer nbre_cas_positifs;
    @XmlElement(required = true)
    protected Integer nbre_cas_importes;
    @XmlElement(required = true)
    protected Integer nbre_deces;
    @XmlElement(required = true)
    protected Integer nbre_gueris;
    @XmlElement(required = true)
    protected XMLGregorianCalendar date;

    /**
     * Obtient la valeur de la propriété nbrtest.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public Integer getNbre_tests_effectues() {
        return nbre_tests_effectues;
    }

    /**
     * Définit la valeur de la propriété nbrtest.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNbre_tests_effectues(Integer value) {
        this.nbre_tests_effectues = value;
    }

    /**
     * Obtient la valeur de la propriété postifcase.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public Integer getNbre_cas_positifs() {
        return nbre_cas_positifs;
    }

    /**
     * Définit la valeur de la propriété postifcase.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNbre_cas_positifs(Integer value) {
        this.nbre_cas_positifs = value;
    }

    /**
     * Obtient la valeur de la propriété importedCase.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public Integer getNbre_cas_importes() {
        return nbre_cas_importes;
    }

    /**
     * Définit la valeur de la propriété importedCase.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNbre_cas_importes(Integer value) {
        this.nbre_cas_importes = value;
    }

    /**
     * Obtient la valeur de la propriété death.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public Integer getNbre_deces() {
        return nbre_deces;
    }

    /**
     * Définit la valeur de la propriété death.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNbre_deces(Integer value) {
        this.nbre_deces = value;
    }

    /**
     * Obtient la valeur de la propriété recovered.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public Integer getNbre_gueris() {
        return nbre_gueris;
    }

    /**
     * Définit la valeur de la propriété recovered.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNbre_gueris(Integer value) {
        this.nbre_gueris = value;
    }

    /**
     * Obtient la valeur de la propriété date.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getDate() {
        return date;
    }

    /**
     * Définit la valeur de la propriété date.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setDate(XMLGregorianCalendar value) {
        this.date = value;
    }

}
