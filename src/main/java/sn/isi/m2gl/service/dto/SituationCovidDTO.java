package sn.isi.m2gl.service.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link sn.isi.m2gl.domain.SituationCovid} entity.
 */
public class SituationCovidDTO implements Serializable {
    
    private Long id;

    @NotNull
    private Integer nbre_tests_effectues;

    @NotNull
    private Integer nbre_cas_positifs;

    @NotNull
    private Integer nbre_cas_importes;

    @NotNull
    private Integer nbre_deces;

    @NotNull
    private Integer nbre_gueris;

    @NotNull
    private ZonedDateTime date;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNbre_tests_effectues() {
        return nbre_tests_effectues;
    }

    public void setNbre_tests_effectues(Integer nbre_tests_effectues) {
        this.nbre_tests_effectues = nbre_tests_effectues;
    }

    public Integer getNbre_cas_positifs() {
        return nbre_cas_positifs;
    }

    public void setNbre_cas_positifs(Integer nbre_cas_positifs) {
        this.nbre_cas_positifs = nbre_cas_positifs;
    }

    public Integer getNbre_cas_importes() {
        return nbre_cas_importes;
    }

    public void setNbre_cas_importes(Integer nbre_cas_importes) {
        this.nbre_cas_importes = nbre_cas_importes;
    }

    public Integer getNbre_deces() {
        return nbre_deces;
    }

    public void setNbre_deces(Integer nbre_deces) {
        this.nbre_deces = nbre_deces;
    }

    public Integer getNbre_gueris() {
        return nbre_gueris;
    }

    public void setNbre_gueris(Integer nbre_gueris) {
        this.nbre_gueris = nbre_gueris;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SituationCovidDTO)) {
            return false;
        }

        return id != null && id.equals(((SituationCovidDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SituationCovidDTO{" +
            "id=" + getId() +
            ", nbre_tests_effectues=" + getNbre_tests_effectues() +
            ", nbre_cas_positifs=" + getNbre_cas_positifs() +
            ", nbre_cas_importes=" + getNbre_cas_importes() +
            ", nbre_deces=" + getNbre_deces() +
            ", nbre_gueris=" + getNbre_gueris() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
