package sn.isi.m2gl.service.mapper;


import sn.isi.m2gl.domain.*;
import sn.isi.m2gl.service.dto.SituationCovidDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link SituationCovid} and its DTO {@link SituationCovidDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SituationCovidMapper extends EntityMapper<SituationCovidDTO, SituationCovid> {



    default SituationCovid fromId(Long id) {
        if (id == null) {
            return null;
        }
        SituationCovid situationCovid = new SituationCovid();
        situationCovid.setId(id);
        return situationCovid;
    }
}
