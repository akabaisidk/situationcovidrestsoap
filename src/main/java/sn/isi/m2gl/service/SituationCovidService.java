package sn.isi.m2gl.service;

import sn.isi.m2gl.service.dto.SituationCovidDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.isi.m2gl.domain.SituationCovid}.
 */
public interface SituationCovidService {

    /**
     * Save a situationCovid.
     *
     * @param situationCovidDTO the entity to save.
     * @return the persisted entity.
     */
    SituationCovidDTO save(SituationCovidDTO situationCovidDTO);

    /**
     * Get all the situationCovids.
     *
     * @return the list of entities.
     */
    List<SituationCovidDTO> findAll();


    /**
     * Get the "id" situationCovid.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SituationCovidDTO> findOne(Long id);

    /**
     * Delete the "id" situationCovid.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
