package sn.isi.m2gl.service.impl;

import sn.isi.m2gl.service.SituationCovidService;
import sn.isi.m2gl.domain.SituationCovid;
import sn.isi.m2gl.repository.SituationCovidRepository;
import sn.isi.m2gl.service.dto.SituationCovidDTO;
import sn.isi.m2gl.service.mapper.SituationCovidMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link SituationCovid}.
 */
@Service
@Transactional
public class SituationCovidServiceImpl implements SituationCovidService {

    private final Logger log = LoggerFactory.getLogger(SituationCovidServiceImpl.class);

    private final SituationCovidRepository situationCovidRepository;

    private final SituationCovidMapper situationCovidMapper;

    public SituationCovidServiceImpl(SituationCovidRepository situationCovidRepository, SituationCovidMapper situationCovidMapper) {
        this.situationCovidRepository = situationCovidRepository;
        this.situationCovidMapper = situationCovidMapper;
    }

    @Override
    public SituationCovidDTO save(SituationCovidDTO situationCovidDTO) {
        log.debug("Request to save SituationCovid : {}", situationCovidDTO);
        SituationCovid situationCovid = situationCovidMapper.toEntity(situationCovidDTO);
        situationCovid = situationCovidRepository.save(situationCovid);
        return situationCovidMapper.toDto(situationCovid);
    }

    @Override
    @Transactional(readOnly = true)
    public List<SituationCovidDTO> findAll() {
        log.debug("Request to get all SituationCovids");
        return situationCovidRepository.findAll().stream()
            .map(situationCovidMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<SituationCovidDTO> findOne(Long id) {
        log.debug("Request to get SituationCovid : {}", id);
        return situationCovidRepository.findById(id)
            .map(situationCovidMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete SituationCovid : {}", id);
        situationCovidRepository.deleteById(id);
    }
}
