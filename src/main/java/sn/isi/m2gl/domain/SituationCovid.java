package sn.isi.m2gl.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A SituationCovid.
 */
@Entity
@Table(name = "situation_covid")
public class SituationCovid implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nbre_tests_effectues", nullable = false)
    private Integer nbre_tests_effectues;

    @NotNull
    @Column(name = "nbre_cas_positifs", nullable = false)
    private Integer nbre_cas_positifs;

    @NotNull
    @Column(name = "nbre_cas_importes", nullable = false)
    private Integer nbre_cas_importes;

    @NotNull
    @Column(name = "nbre_deces", nullable = false)
    private Integer nbre_deces;

    @NotNull
    @Column(name = "nbre_gueris", nullable = false)
    private Integer nbre_gueris;

    @NotNull
    @Column(name = "date", nullable = false)
    private ZonedDateTime date;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNbre_tests_effectues() {
        return nbre_tests_effectues;
    }

    public SituationCovid nbre_tests_effectues(Integer nbre_tests_effectues) {
        this.nbre_tests_effectues = nbre_tests_effectues;
        return this;
    }

    public void setNbre_tests_effectues(Integer nbre_tests_effectues) {
        this.nbre_tests_effectues = nbre_tests_effectues;
    }

    public Integer getNbre_cas_positifs() {
        return nbre_cas_positifs;
    }

    public SituationCovid nbre_cas_positifs(Integer nbre_cas_positifs) {
        this.nbre_cas_positifs = nbre_cas_positifs;
        return this;
    }

    public void setNbre_cas_positifs(Integer nbre_cas_positifs) {
        this.nbre_cas_positifs = nbre_cas_positifs;
    }

    public Integer getNbre_cas_importes() {
        return nbre_cas_importes;
    }

    public SituationCovid nbre_cas_importes(Integer nbre_cas_importes) {
        this.nbre_cas_importes = nbre_cas_importes;
        return this;
    }

    public void setNbre_cas_importes(Integer nbre_cas_importes) {
        this.nbre_cas_importes = nbre_cas_importes;
    }

    public Integer getNbre_deces() {
        return nbre_deces;
    }

    public SituationCovid nbre_deces(Integer nbre_deces) {
        this.nbre_deces = nbre_deces;
        return this;
    }

    public void setNbre_deces(Integer nbre_deces) {
        this.nbre_deces = nbre_deces;
    }

    public Integer getNbre_gueris() {
        return nbre_gueris;
    }

    public SituationCovid nbre_gueris(Integer nbre_gueris) {
        this.nbre_gueris = nbre_gueris;
        return this;
    }

    public void setNbre_gueris(Integer nbre_gueris) {
        this.nbre_gueris = nbre_gueris;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public SituationCovid date(ZonedDateTime date) {
        this.date = date;
        return this;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SituationCovid)) {
            return false;
        }
        return id != null && id.equals(((SituationCovid) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SituationCovid{" +
            "id=" + getId() +
            ", nbre_tests_effectues=" + getNbre_tests_effectues() +
            ", nbre_cas_positifs=" + getNbre_cas_positifs() +
            ", nbre_cas_importes=" + getNbre_cas_importes() +
            ", nbre_deces=" + getNbre_deces() +
            ", nbre_gueris=" + getNbre_gueris() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
